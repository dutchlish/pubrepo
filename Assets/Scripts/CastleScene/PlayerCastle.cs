﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class PlayerCastle : MonoBehaviour
{
    //floats and ints
    public float speed;
    private float drunkPerCent;
    private float cash;
    private float swayAdjust = 10000;

    //booleans
    bool canDance;
    public bool canMove;
    bool canDrink;

    //animator and animations
    public Animator playerAnim;

    //ui
    public TextMeshProUGUI drunkPerCentText, cashText;
    public Slider drunkSlider;

    //Game Objects
    public GameObject speaker;
    public GameObject bouncer;
    public GameObject playerContainer;
    public GameObject beerMenu;
    public GameObject dancer1, dancer2;

    private static GameObject dancer1instance;
    private static GameObject dancer2instance;
    //sounds
    public GameObject drinkingSound;
    public GameObject[] knockableObjects;

    

    //CinemachineCameras
    [SerializeField]
    private CinemachineVirtualCamera firstCam, backroomCam, barmaidCam, stairwellCam, upstairsCam, minigame1Cam;

    // Start is called before the first frame update

    void Start()
    {
       
        drunkPerCent = 0;
        cash = 75;
      
        playerAnim = GetComponentInChildren<Animator>();
        //gameObject caches - still to be removed and applied in the inspector
        speaker = GameObject.Find("Speaker");
        drinkingSound = GameObject.Find("DrinkingSound");
    
        bouncer = GameObject.Find("Bouncer");
        playerContainer = GameObject.Find("PlayerContainer");

        canMove = true;
      
        canDrink = false;

        foreach (GameObject obj in knockableObjects)
        {
            obj.GetComponent<Rigidbody>().isKinematic = true;
        }
    }


    void Update()
    {
       
        ReduceDrunk();
        Sway();
        ShowDrunkUI();
        ShowCashUI();
        Reload();
        DanceIfAppropriate();
        ShowBeerMenu();
        DoCardTrick();
        CheckIfDrunkEnoughToAnimateDrunk();
        AnimatePlayerWalk();
        MakeMoveAgain();
                  
    }

    private void FixedUpdate()
    {
        RotatePlayer();
        MovePlayer();
       
    }

    //functions

        //make player sway if drunk
    void Sway()
    {
        float sway = Random.Range(0, 3);
        if (sway > 1.5) transform.Translate(drunkPerCent / swayAdjust, 0, 0);
        if (sway < 1.5) transform.Translate(-drunkPerCent / sway, 0, 0);
    }
    //sober up over time
    void ReduceDrunk()
    {
        if (drunkPerCent >= 0)
        {
            if (canMove)
            {
                drunkPerCent -= Time.deltaTime / 4;
                    firstCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain -= Time.deltaTime / 40;
                    backroomCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain -= Time.deltaTime / 40;
                    barmaidCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain -= Time.deltaTime / 40;
                    stairwellCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain -= Time.deltaTime / 40;
                    upstairsCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain -= Time.deltaTime / 40;
                }
            }
       }
    //show the drinking UI
    void ShowDrunkUI()
    {
        drunkPerCentText.text = "Inebriation: " + drunkPerCent.ToString("0") + "%";
        drunkSlider.value = drunkPerCent;
    }

    void Reload()
    {
        if (Input.GetKey(KeyCode.R))
        {

            SceneManager.LoadScene("CastleScene");

        }
    }
    //make player Dance
    void DanceIfAppropriate()
    {
        if (canMove)
        {
            if (Input.GetKey(KeyCode.Space))
            {

                playerAnim.SetBool("isDancing", true);
                Debug.Log("Dance!");

            }

            if (Input.GetKeyUp(KeyCode.Space))
            {
                playerAnim.SetBool("isDancing", false);
            }
        }
    }
   
    void ShowBeerMenu()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (canDrink)
            {
                beerMenu.gameObject.SetActive(true);
            }
        }
    }

    void DoCardTrick()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            StartCoroutine(DoMagic());
        }
    }

    void CheckIfDrunkEnoughToAnimateDrunk()
    {
        if (drunkPerCent > 30)
        {
            playerAnim.SetBool("isDrunk", true);
            speed = 2.5f;
            foreach(GameObject obj in knockableObjects)
            {
                obj.GetComponent<Rigidbody>().isKinematic = false;
                obj.GetComponent<Rigidbody>().mass = 0.01f;

            }
        }
        if (drunkPerCent < 30)
        {
            playerAnim.SetBool("isDrunk", false);
            speed = 5;
            foreach (GameObject obj in knockableObjects)
            {
                obj.GetComponent<Rigidbody>().isKinematic = true;
                obj.GetComponent<Rigidbody>().mass = 1f;
            }
        }
        {
        if (Input.GetKeyDown(KeyCode.M))
        {
            StartCoroutine(DoMagic());
        }
    }

    }

    void RotatePlayer()
    {
        if (canMove)
        {
            if (Input.GetKey(KeyCode.D))
            {
                transform.Rotate(0, 2, 0 * Time.deltaTime);
            }
            if (Input.GetKeyUp(KeyCode.D))
            {
                transform.Rotate(0, 0, 0);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.Rotate(0, -2, 0 * Time.deltaTime);
            }
            if (Input.GetKeyUp(KeyCode.A))
            {
                transform.Rotate(0, 0, 0);
            }
        }
    }

    void MovePlayer()
    {
        if (canMove)
        {
            float translation = Input.GetAxis("Vertical") * speed;
            float straffe = Input.GetAxis("Horizontal") * speed;
            float jump = Input.GetAxis("Jump") * 2;
            translation *= Time.deltaTime;
            straffe *= Time.deltaTime;
            jump *= Time.deltaTime;
            transform.Translate(0, 0, translation);
        }
    }

    void AnimatePlayerWalk()
    {
        if (Input.GetAxis("Vertical") > 0)
        {
            playerAnim.SetInteger("WalkDirection", 1);

        }
        if (Input.GetAxis("Vertical") == 0) playerAnim.SetInteger("WalkDirection", 0);
    }

    void MakeMoveAgain()
    {
        if (Input.GetKey(KeyCode.X))
        {
            canMove = true;
            
            minigame1Cam.Priority = 5;
          
        }
    }

    public void DrinkBeer()
    {
        if (drunkPerCent <= 90)
        {
            beerMenu.SetActive(false);
            StartCoroutine(DrinkBeerCoroutine());
        }
    }

    public void DrinkStrongBeer1()
    {
        if (drunkPerCent <= 85)
        {
            beerMenu.SetActive(false);
            StartCoroutine(DrinkStrongBeer1Coroutine());
        }

    }

    public void DrinkStrongBeer2()
    {
        if (drunkPerCent <= 80)
        {
            beerMenu.SetActive(false);
            StartCoroutine(DrinkStrongBeer2Coroutine());
        }
    }

    public void DrinkStrongBeer3()
    {
        if (drunkPerCent <= 75)
        {
            beerMenu.SetActive(false);
            StartCoroutine(DrinkStrongBeer3Coroutine());
        }
    }

    void ShowCashUI()
    {
        cashText.text = "CASH: £" + cash.ToString("");
    }

    //collison and trigger checks

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Dancer1"))
        {
            dancer1instance = other.gameObject;
            dancer1instance.GetComponent<Animator>().SetTrigger("IsAngry");
        }
        if (other.gameObject.CompareTag("Dancer2"))
        {
            dancer2instance = other.gameObject;
            dancer1instance.GetComponent<Animator>().SetTrigger("IsAngry");
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Backroom"))
        {
            backroomCam.Priority = 15;
        }
        if (other.gameObject.CompareTag("Barmaid"))
        {
            canDrink = true;
            barmaidCam.Priority = 15;
        }
        if (other.gameObject.CompareTag("Stairwell"))
        {
            stairwellCam.Priority = 15;
        }
        if (other.gameObject.CompareTag("Upstairs"))
        {
            upstairsCam.Priority = 15;
        }
        if (other.gameObject.CompareTag("Minigame1"))
        {
            canMove = false;
            minigame1Cam.Priority = 15;         
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Backroom"))
        {
            backroomCam.Priority = 5;
        }
        if (other.gameObject.CompareTag("Barmaid"))
        {
            canDrink = false;
            barmaidCam.Priority = 5;
        }
        if (other.gameObject.CompareTag("Stairwell"))
        {
            stairwellCam.Priority = 5;
        }
        if (other.gameObject.CompareTag("Upstairs"))
        {
            upstairsCam.Priority = 5;
        }
        if (other.gameObject.CompareTag("Minigame1"))
        {
            canMove = true;
            minigame1Cam.Priority = 5;     
        }
    }

   
    //CORoutines

    public IEnumerator DrinkBeerCoroutine()
    {
        playerAnim.SetBool("Drinking", true);
        yield return new WaitForSeconds(3f);
        drinkingSound.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2f);
        playerAnim.SetBool("Drinking", false);
        firstCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1f;
        backroomCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1f;
        barmaidCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1f;
        stairwellCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1f;
        upstairsCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1f;
        //count up drunk percent over time
        int i;
        for (i = 0; i < 10; i++)
        {
            drunkPerCent += 1f;
            yield return new WaitForSeconds(0.03f);
        }
        //count down cash over time
        int j;
        for(j = 3; j > 0; j--)
        {
            cash--;
            yield return new WaitForSeconds(0.1f);
        }  

    }

    public IEnumerator DrinkStrongBeer1Coroutine()
    {
        playerAnim.SetBool("Drinking", true);
        yield return new WaitForSeconds(3f);
        drinkingSound.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2f);
        playerAnim.SetBool("Drinking", false);
        firstCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1.5f;
        backroomCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1.5f;
        barmaidCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1.5f;
        stairwellCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1.5f;
        upstairsCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 1.5f;
        int i;
        for (i = 0; i < 15; i++)
        {
            drunkPerCent += 1f;
            yield return new WaitForSeconds(0.03f);
        }
        int j;
        for (j = 5; j > 0; j--)
        {
            cash--;
            yield return new WaitForSeconds(0.1f);
        }
        //drunkPerCent += 15;

    }

    public IEnumerator DrinkStrongBeer2Coroutine()
    {
        playerAnim.SetBool("Drinking", true);
        yield return new WaitForSeconds(3f);
        drinkingSound.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2f);
        playerAnim.SetBool("Drinking", false);
        firstCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2f;
        backroomCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2f;
        barmaidCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2f;
        stairwellCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2f;
        upstairsCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2f;
        int i;
        for (i = 0; i < 20; i++)
        {
            drunkPerCent += 1f;
            yield return new WaitForSeconds(0.03f);
        }
        float j;
        for (j = 6.50f; j > 0f; j--)
        {
            cash--;
            yield return new WaitForSeconds(0.1f);
        }
        cash += 0.5f;
        // drunkPerCent += 20;

    }

    public IEnumerator DrinkStrongBeer3Coroutine()
    {
        playerAnim.SetBool("Drinking", true);
        yield return new WaitForSeconds(3f);
        drinkingSound.GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2f);
        playerAnim.SetBool("Drinking", false);
        firstCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2.5f;
        backroomCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2.5f;
        barmaidCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2.5f;
        stairwellCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2.5f;
        upstairsCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain += 2.5f;
        int i;
        for (i = 0; i < 25; i ++)
        {
            drunkPerCent += 1f;
            yield return new WaitForSeconds(0.03f);
        }
        int j;
        for (j = 8; j > 0; j--)
        {
            cash--;
            yield return new WaitForSeconds(0.1f);
        }
        
        //   drunkPerCent += 25;

    }

    IEnumerator DoMagic()
    {
        playerAnim.SetBool("DoingMagic", true);
        yield return new WaitForSeconds(15);
        playerAnim.SetBool("DoingMagic", false);
    }



    IEnumerator Fall()
    {
        Vector3 position = new Vector3(playerContainer.transform.position.x, playerContainer.transform.position.y, playerContainer.transform.position.z);
        Vector3 newPosition = new Vector3(playerContainer.transform.position.x, playerContainer.transform.position.y - 2f, playerContainer.transform.position.z);
        playerContainer.transform.position = newPosition;
        playerAnim.SetTrigger("Fall");
        bouncer.GetComponent<Animator>().SetTrigger("Push");
        yield return new WaitForSeconds(3f);
        playerContainer.transform.position = new Vector3(newPosition.x, newPosition.y + 2f, newPosition.z);
    
    }
}
